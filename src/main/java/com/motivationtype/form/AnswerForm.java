package com.motivationtype.form;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class AnswerForm {

	@NotNull
	private String answer1;
	@NotNull
	private String answer2;

}
