package com.motivationtype.question;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

@EnableJpaRepositories
@Service
public class QuestionService {

	@Autowired //リポジトリを紐づけます
	QuestionRepository repository;

    public Optional<Question> selectFromId(int id) {
    	Optional<Question> question = repository.findById(id);
		return question;
    }
}
