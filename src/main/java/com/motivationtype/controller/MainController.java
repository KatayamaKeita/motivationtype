package com.motivationtype.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.motivationtype.answer.Answer;
import com.motivationtype.answer.AnswerService;
import com.motivationtype.form.AnswerForm;
import com.motivationtype.question.Question;
import com.motivationtype.question.QuestionService;

@EnableJpaRepositories
@Controller
public class MainController {



	@Autowired
	QuestionService questionService;

	@Autowired
	AnswerService answerService;

	@Autowired
	HttpSession session;

	@RequestMapping(value = "/top", method = RequestMethod.GET)
	public String top() {
		session.setAttribute("A", 0);
		session.setAttribute("B", 0);
		session.setAttribute("C", 0);
		session.setAttribute("D", 0);
		return "index";
	}

	@RequestMapping(value = "/top", method = RequestMethod.POST)
	public String nextPage() {
		return "/question/1";
	}

	@RequestMapping(value = "/question/{id}", method = RequestMethod.GET)
    public String question(Model model, @PathVariable int id) {
		Question question = questionService.selectFromId(id).get();
		Iterable<Answer> answers = answerService.selectByQustionId(id);
		model.addAttribute("answers", answers);
		model.addAttribute("question", question);
		return "question";
    }

	@RequestMapping(value = "/question/{id}", method = RequestMethod.POST)
    public String getAnswer(@ModelAttribute AnswerForm answerForm, Model model, @PathVariable int id) {
		id++;
		session.setAttribute(answerForm.getAnswer1(), (int)session.getAttribute(answerForm.getAnswer1()) + 2);
		session.setAttribute(answerForm.getAnswer2(), (int)session.getAttribute(answerForm.getAnswer2()) + 1);
		if(id<=10) {
			return "redirect:/question/"+id;
		}else {
			return "redirect:/result";
		}
    }

	@RequestMapping(value = "/result", method = RequestMethod.GET)
    public String result(Model model) {
		int max = 0;
		int a = (int)session.getAttribute("A");
		int b = (int)session.getAttribute("B");
		int c = (int)session.getAttribute("C");
		int d = (int)session.getAttribute("D");
		int[] result = {a,b,c,d};
		for(int i=0; i < result.length; i++) {
			max = Math.max(max,result[i]);
		}
		List<String> types = new ArrayList<String>();
		if(max == a) {
			types.add("ドライブタイプ");
		}
		if(max == b) {
			types.add("アナライズタイプ");
		}
		if(max == c) {
			types.add("クリエイトタイプ");
		}
		if(max == d) {
			types.add("ボランティアタイプ");
		}
		model.addAttribute("result",types);
		return "result";
    }
}
