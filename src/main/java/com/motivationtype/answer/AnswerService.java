package com.motivationtype.answer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

@EnableJpaRepositories
@Service
public class AnswerService {

	@Autowired
	AnswerRepository repository;

    public Iterable<Answer> selectByQustionId(int questionId) {
		Iterable<Answer> answers = repository.findByQuestionIdEquals(questionId);
		return answers;
    }
}
