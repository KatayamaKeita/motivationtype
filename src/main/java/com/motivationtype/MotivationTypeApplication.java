package com.motivationtype;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MotivationTypeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MotivationTypeApplication.class, args);
    }
}
